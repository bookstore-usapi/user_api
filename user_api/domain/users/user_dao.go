// Unique point to interact with the DB

package users

import (
	"fmt"
	"strings"

	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/loggers"
	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/mysql_utils"
	errors "github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/rest_errors"
	"gitlab.com/josafat.vargas.gamboa/user_api/user_api/db/mysql/users_db"
)

const (
	queryDeleteUser             = "DELETE FROM users WHERE id=?;"
	queryGetUser                = "SELECT id, first_name, last_name, email, date_created FROM users WHERE id=?;"
	queryInsertUser             = "INSERT INTO users(first_name, last_name, email, date_created, status, password) VALUES (?, ?, ?, ?, ?, ?);"
	queryUpdateUser             = "UPDATE users SET first_name=?, last_name=?, email=? WHERE id=?;"
	queryFindByStatus           = "SELECT id, first_name, last_name, email, date_created FROM users WHERE status=?;"
	queryFindByEmailAndPassword = "SELECT id, first_name, last_name, email, status FROM users WHERE email=? AND password=? AND status=?;"
)

var usersDB = make(map[int64]*User)

func (user *User) DB_Get() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryGetUser)
	if err != nil {
		loggers.Error("+++ Error preparing GET statement +++", err)
		return errors.NewInternalServerError("Database error", err)
	}
	defer stmt.Close()

	result := stmt.QueryRow(user.Id)

	if sqlErr := result.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated); sqlErr != nil {
		loggers.Error("+++ SQL error: Scanning failed +++", sqlErr)
		return mysql_utils.ParseError(sqlErr)
	}

	return nil
}

func (user *User) DB_Save() *errors.RestErr {

	// Make sure the query is OK before accessing the DB
	stmt, err := users_db.Client.Prepare(queryInsertUser)
	if err != nil {
		loggers.Error("+++ Error preparing SAVE statement +++", err)
		return errors.NewInternalServerError("Database error", err)
	}
	defer stmt.Close()

	// Insert information to the DB
	insertResult, sqlErr := stmt.Exec(user.FirstName, user.LastName, user.Email, user.DateCreated, user.Status, user.Password)
	if sqlErr != nil {
		loggers.Error("+++ SQL error: Insert failed +++", sqlErr)
		return mysql_utils.ParseError(sqlErr)
	}

	userId, err := insertResult.LastInsertId()
	if err != nil {
		loggers.Error("+++ SQL error: Error retrieving last inserted ID +++", sqlErr)
		return mysql_utils.ParseError(sqlErr)
	}

	user.Id = userId
	return nil
}

func (user *User) DB_Update() *errors.RestErr {

	// Make sure the query is OK before accessing the DB
	stmt, err := users_db.Client.Prepare(queryUpdateUser)
	if err != nil {
		loggers.Error("+++ Error preparing UPDATE statement +++", err)
		return errors.NewInternalServerError("Database error", err)
	}
	defer stmt.Close()

	_, sqlErr := stmt.Exec(user.FirstName, user.LastName, user.Email, user.Id)
	if sqlErr != nil {
		loggers.Error("+++ SQL error: Update failed+++", sqlErr)
		return mysql_utils.ParseError(sqlErr)
	}

	return nil

}

func (user *User) DB_Delete() *errors.RestErr {
	// Make sure the query is OK before accessing the DB
	stmt, err := users_db.Client.Prepare(queryDeleteUser)
	if err != nil {
		loggers.Error("+++ Error preparing DELETE statement +++", err)
		return errors.NewInternalServerError("Database error", err)
	}
	defer stmt.Close()

	_, sqlErr := stmt.Exec(user.Id)
	if sqlErr != nil {
		loggers.Error("+++ SQL error: Delete failed +++", sqlErr)
		return mysql_utils.ParseError(sqlErr)
	}

	return nil
}

func (user *User) DB_FindByStatus(status string) ([]User, *errors.RestErr) {
	// Make sure the query is OK before accessing the DB
	stmt, err := users_db.Client.Prepare(queryFindByStatus)
	if err != nil {
		loggers.Error("+++ Error preparing FIND statement +++", err)
		return nil, errors.NewInternalServerError("Database error", err)
	}
	defer stmt.Close()

	// Get rows from the DB, defer release connection
	rows, sqlErr := stmt.Query(status)
	if sqlErr != nil {
		loggers.Error("+++ SQL error: Find failed+++", sqlErr)
		return nil, mysql_utils.ParseError(sqlErr)
	}
	defer rows.Close()

	results := make([]User, 0)

	for rows.Next() {
		var user User
		if err := rows.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated); err != nil {
			loggers.Error("+++ SQL error: Parse failed+++", sqlErr)
			return nil, mysql_utils.ParseError(sqlErr)
		}
		results = append(results, user)
	}

	if len(results) == 0 {
		loggers.Error("+++ SQL error: No users found +++", sqlErr)
		return nil, errors.NewNotFoundError(fmt.Sprintf("No users found with status %s", status))
	}

	return results, nil

}

func (user *User) DB_FindByEmailAndPassword() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryFindByEmailAndPassword)
	if err != nil {
		loggers.Error("+++ Error preparing FIND statement +++", err)
		return errors.NewInternalServerError("Database error", err)
	}
	defer stmt.Close()

	result := stmt.QueryRow(user.Email, user.Password, StatusActive)

	if sqlErr := result.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated); sqlErr != nil {
		if strings.Contains(sqlErr.Error(), mysql_utils.ErrorNoRows) {
			return errors.NewNotFoundError("Invalid user credentials")
		}
		loggers.Error("+++ SQL error: Scanning failed +++", sqlErr)
		return mysql_utils.ParseError(sqlErr)
	}

	return nil
}
