package users_db

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

const (
	mysql_users_username = "my_sql_users_username"
	mysql_users_password = "my_sql_users_password"
	mysql_users_host     = "my_sql_users_host"
	mysql_users_schema   = "my_sql_users_schema"
)

var Client *sql.DB

var (
	username = "root"           // os.Getenv(mysql_users_username)
	password = "Pa$$w0rd"       // os.Getenv(mysql_users_password)
	host     = "localhost:3306" // os.Getenv(mysql_users_host)
	schema   = "user_db"        // os.Getenv(mysql_users_schema)
)

// TODO: redefine call as an interface for easier mocking

func init() {
	sourceName := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8",
		username, password, host, schema,
	)

	var err error
	Client, err = sql.Open("mysql", sourceName)
	if err != nil {
		panic(err) // Can cause Error 1045: Access denied for user @localhost
	}

	if err = Client.Ping(); err != nil {
		panic(err) // Can cause Error 1045: Access denied for user @localhost
	}

	// mysql.SetLogger("")
	log.Println("Database successfully configured")
}
