package main

import (
	"gitlab.com/josafat.vargas.gamboa/user_api/user_api/app"
)

func main() {
	app.StartApplication()
}