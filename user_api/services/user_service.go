package services

import (
	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/cryptos"
	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/dates"
	errors "github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/rest_errors"
	"gitlab.com/josafat.vargas.gamboa/user_api/user_api/domain/users"
)

var UsersService userServiceInterface = &usersService{}

type usersService struct{}

type userServiceInterface interface {
	CreateUser(users.User) (*users.User, *errors.RestErr)
	GetUser(int64) (*users.User, *errors.RestErr)
	UpdateUser(bool, users.User) (*users.User, *errors.RestErr)
	DeleteUser(int64) *errors.RestErr
	FindByStatus(string) (users.Users, *errors.RestErr)
	FindByEmailAndPassword(users.LoginRequest) (*users.User, *errors.RestErr)
}

// Business logic. Has no access to the HTTP server or DB

func (s *usersService) CreateUser(user users.User) (*users.User, *errors.RestErr) {
	if err := user.Validate(); err != nil {
		return nil, err
	}

	user.DateCreated = dates.GetNowDBFormat()
	user.Status = users.StatusActive
	user.Password = cryptos.GetMd5(user.Password)

	if err := user.DB_Save(); err != nil {
		return nil, err
	}

	return &user, nil
}

func (s *usersService) GetUser(userId int64) (*users.User, *errors.RestErr) {
	if userId <= 0 {
		return nil, errors.NewBadRequestError("Invalid user Id (value out of range)")
	}
	read_user := users.User{Id: userId}
	if err := read_user.DB_Get(); err != nil {
		return nil, err
	}
	return &read_user, nil

}

func (s *usersService) UpdateUser(isPartial bool, user users.User) (*users.User, *errors.RestErr) {
	current, err := s.GetUser(user.Id)
	if err != nil {
		return nil, err
	}

	// PATCH / PUT
	if isPartial {
		if user.FirstName != "" {
			current.FirstName = user.FirstName
		}
		if user.LastName != "" {
			current.LastName = user.LastName
		}
		if user.Email != "" {
			current.Email = user.Email
		}
	} else {
		current.FirstName = user.FirstName
		current.LastName = user.LastName
		current.Email = user.Email
	}

	if err := current.DB_Update(); err != nil {
		return nil, err
	}

	return current, nil

}

func (s *usersService) DeleteUser(userId int64) *errors.RestErr {
	user := &users.User{Id: userId}
	return user.DB_Delete()
}

func (s *usersService) FindByStatus(status string) (users.Users, *errors.RestErr) {
	user := &users.User{}
	return user.DB_FindByStatus(status)
}

func (s *usersService) FindByEmailAndPassword(request users.LoginRequest) (*users.User, *errors.RestErr) {
	user := &users.User{
		Email:    request.Email,
		Password: cryptos.GetMd5(request.Password),
	}
	if err := user.DB_FindByEmailAndPassword(); err != nil {
		return nil, err
	}
	return user, nil
}
