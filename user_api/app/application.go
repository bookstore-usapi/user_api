package app

import (
	"github.com/gin-gonic/gin"
	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/loggers"
)

var router = gin.Default()

func StartApplication() {
	mapUrls()
	loggers.Info("+++ Starting application +++")
	router.Run(":9000")
}
