package app

import (
	ping "gitlab.com/josafat.vargas.gamboa/user_api/user_api/controllers/ping"
	user "gitlab.com/josafat.vargas.gamboa/user_api/user_api/controllers/user"
)

func mapUrls() {
	router.GET("/ping", ping.Ping)

	router.POST("/users", user.CreateUser)
	router.GET("/users/:user_id", user.GetUser)
	router.PUT("/users/:user_id", user.UpdateUser)
	router.PATCH("/users/:user_id", user.UpdateUser)
	router.DELETE("/users/:user_id", user.DeleteUser)
	router.GET("/internal/users/search", user.SearchUser)
	router.POST("/users/login", user.Login)

}
